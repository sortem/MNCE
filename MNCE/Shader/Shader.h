//
//  Shader.h
//  MNCE
//
//  Created by pavel on 24.04.15.
//
//

#ifndef __MNCE__Shader__
#define __MNCE__Shader__

#define VERTEX_SHADER 0
#define FRAGMENT_SHADER 1

#include "Define.h"

class Shader {
public:
    Shader(const std::string& fileNmae);
    void Bind();
    virtual ~Shader();
private:
    static const unsigned int NUM_SHADERS = 2; // deal with geometry shader NUM_SHADERS = 3;
    Shader(const Shader& other){}
    void operator=(Shader& other) {}
    GLuint m_program;
    GLuint m_shaders[];
};

#endif /* defined(__MNCE__Shader__) */
