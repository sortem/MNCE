//
//  Shader.cpp
//  MNCE
//
//  Created by pavel on 24.04.15.
//
//

#include "Shader.h"
#include <fstream>

using namespace std;

static string LoadShader(const string& fileName);
static void CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const string& errorMessage);
static GLuint CreateShader(const string& text, GLenum shaderType);

Shader::Shader(const string& fileName) {
    m_program = glCreateProgram();
    m_shaders[VERTEX_SHADER] = CreateShader(LoadShader(fileName + ".vs"), GL_VERTEX_SHADER);
    m_shaders[FRAGMENT_SHADER] = CreateShader(LoadShader(fileName + ".fs"), GL_FRAGMENT_SHADER);

    for (unsigned int i = 0; i < NUM_SHADERS; i++) {
        glAttachShader(m_program, m_shaders[i]);
        glBindAttribLocation(m_program, 0, "postition");
        glLinkProgram(m_program);
        CheckShaderError(m_program, GL_LINK_STATUS, true, "Error: Program linking failed: ");
        glValidateProgram(m_program);
        CheckShaderError(m_program, GL_VALIDATE_STATUS, true, "Error: Program validation failed: ");
    }
}

Shader::~Shader() {
    for (unsigned int i = 0; i < NUM_SHADERS; i++) {
        glDetachShader(m_program, m_shaders[i]);
        glDeleteShader(m_shaders[i]);
    }
    glDeleteProgram(m_program);
}

static GLuint CreateShader(const string& text, GLenum shaderType) {
    GLuint shader = glCreateShader(shaderType);
    if (shader == 0) {
        cerr << "Error: shader creation failed" << endl;
    }
    
    const GLchar* shaderSource[1];
    GLint shaderSourceStringLength[1];

    shaderSource[0] = text.c_str();
    shaderSourceStringLength[0] = (GLint)text.length();
    
    glShaderSource(shader, 1, shaderSource, shaderSourceStringLength);
    glCompileShader(shader);
    CheckShaderError(shader, GL_COMPILE_STATUS, false, "Error: Shader compilation failed: ");
    
    return shader;
}

void Shader::Bind() {
    glUseProgram(m_program);
}

#pragma mark - shader loading from file

static string LoadShader(const string& fileName) {
    ifstream file;

    //file.open("test.txt");
    file.open((fileName).c_str());
    string output;
    string line;
    if (file.is_open()) {
        while (file.good()) {
            getline(file,line);
            output.append(line + "\n");
        }
    }
    else {
        cerr << "Unable to load shader: " << fileName << endl;
    }
    return output;
}

static void CheckShaderError(GLuint shader, GLenum flag, bool isProgram, const string& errorMessage) {
    GLint success = 0;
    GLchar error[1024] = { 0 };
    
    if (isProgram) {
        glGetProgramiv(shader, flag, &success);
    }
    else {
        glGetShaderiv(shader, flag, &success);
    }
    
    if (success == GL_FALSE) {
        if (isProgram) {
            glGetProgramInfoLog(shader, sizeof(error), NULL, error);
        }
        else {
            glGetShaderInfoLog(shader, sizeof(error), NULL, error);
        }
        cerr << errorMessage << ": " << error << "'" << endl;
    }
}