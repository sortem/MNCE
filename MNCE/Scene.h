//
//  Scene.h
//  MNCE
//
//  Created by pavel on 24.04.15.
//
//

#ifndef __MNCE__Scene__
#define __MNCE__Scene__

#include "Define.h"
#include "Shader.h"

class Scene {
public:
    Scene();
    ~Scene();
    void drawScene();
private:
};

#endif