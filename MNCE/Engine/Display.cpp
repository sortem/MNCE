//
//  Display.cpp
//  MNCE
//
//  Created by pavel on 24.04.15.
//
//

#include "Display.h"

static void error_callback(int error, const char* description)
{
    fputs(description, stderr);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}

Display::Display(int width, int height, const std::string title) {
    glfwSetErrorCallback(error_callback);
    const char * windowName = title.c_str();
    if (!glfwInit())
        exit(EXIT_FAILURE);
    window = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, windowName, NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    glfwSetKeyCallback(window, key_callback);
}

Display::~Display() {
    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}

void Display::SwapBuffers() {
    glfwSwapBuffers(window);
}
