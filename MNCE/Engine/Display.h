//
//  Display.h
//  MNCE
//
//  Created by pavel on 24.04.15.
//
//

#ifndef __MNCE__Display__
#define __MNCE__Display__

#include "Define.h"

class Display {
public:
    Display(int width, int height, const std::string title);
    void SwapBuffers();
    ~Display();
    GLFWwindow *window;
private:
    Display(const Display& other){};
    Display& operator=(const Display& other);
};

#endif 
