//
//  Mesh.h
//  MNCE
//
//  Created by pavel on 24.04.15.
//
//

#ifndef __MNCE__Mesh__
#define __MNCE__Mesh__

#include <stdio.h>
#include "Define.h"
#include "Shader.h"
#include "glm.hpp"

class Vertex {
public:
    Vertex(const glm::vec3& pos) {
        this->pos = pos;
    }
private:
    glm::vec3 pos;
};

class Mesh {
public:
    Mesh(Vertex* verteces, unsigned int numVerteces);
    void draw();
    ~Mesh();
private:
    Mesh(const Mesh& other);
    void operator=(const Mesh& other);
    
    enum {
        POSITION_VB,
        NUM_BUFFERS
    };
    GLuint m_vertexArrayObject;
    GLuint m_vertexArrayBuffers[NUM_BUFFERS];
    unsigned int m_drawCount;
};

#endif
